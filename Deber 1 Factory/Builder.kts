// Constructor Builder

class Persona(private val email: String,
               private var firstName: String = "",
               var lastName: String = "",
               private var _phone: String = "",
               var type: String = "") {

    fun phone(phone: String): Persona {
        _phone = phone
        return this
    }

    class PersonaBuilder(email: String) {
        private var persona: Persona = Persona(email = email, type = "persona")

        fun firstName(firstName: String): PersonaBuilder {
            this.persona.firstName = firstName
            return this
        }

        fun build(): Persona {
            return persona
        }
    }

}


fun main(args: Array<String>) {
    val persona= Persona.PersonaBuilder("nikolyuri@mail.com").firstName("Nicole").build()

    val persona1 = Persona("Email").phone("234323")

    val persona2 = Persona("emaaill").apply {
        type = "new persona"
        lastName = "Pirca"
    }.phone("23434534")
}