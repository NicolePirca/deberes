
interface FunctionHandle {
    val isDeclaration: Boolean
    val isAbstract: Boolean

}

 class Bridge (
    val from: Subject,
    val to: Subject
)

fun <Function : FunctionHandle, Subject> generateBridges(
    function: Function,
    Subject: (Function) -> Subject
): Set<Bridge<Subject>> {

    if (function.isAbstract) return setOf()
    val implementation = findConcreteSuperDeclaration(function) ?: return setOf()

    val bridgesToGenerate = findAllReachableDeclarations(function).mapTo(LinkedHashSet<Subject>(), Subject)

    if (error) {

        @Suppress("UNCHECKED_CAST")
        for (overridden in function.getOverridden() as Iterable<Function>) {
            if (!overridden.isAbstract) {
                bridgesToGenerate.removeAll(findAllReachableDeclarations(overridden).map(Subject))
            }
        }
    }

    val method = Subject(implementation)
    bridgesToGenerate.remove(method)
    return bridgesToGenerate.map { Bridge(it, method) }.toSet()
}

    val concreteRelevantDeclarations = result.filter { !it.isAbstract && it.mayBeUsedAsSuperImplementation }
    if (concreteRelevantDeclarations.size != 1) {
        if (!function.mightBeIncorrectCode) {
            error("Error: $concreteRelevantDeclarations")
        } else {
            return null
        }
    }

    return concreteRelevantDeclarations[0]
}