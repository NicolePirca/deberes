package patterns
// clase inteface
interface CarFunction {
    fun doFunctionCar()
}

interface CarFunctionDecorator : CarFunction

class BasicCarService : CarService {
    override fun doFunctionCar() = println("Class Car Service")
}

class CarStart(private val CarFunction: CarFunction) : CarFunctionDecorator {
    override fun doFunctionCar() {
        CarFunction.doFunctionCar()
        println("function Car service")
    }
}

class CarAccel(private val CarFunction: CarFunction) : CarFunctionDecorator {
    override fun doFunctionCar() {
        CarFunction.doFunctionCar()
        println("Cleaning car inside")
    }
}

fun main(args: Array <String>) {
    val CarFunction = CarAccel(CarStart(BasicCarService()))
    CarFunction.doFunctionCar()
}