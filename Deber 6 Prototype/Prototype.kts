
class Bike : Cloneable {
    private var numBike: Int = 0
    private var tipoModelo: String? = null
    var model: String? = null
        
    init {
        tipoModelo = "Montañera"
        model = "Leopard"
        numBike = 4
    }

    public override fun clone(): Bike {
        return Bike()
    }

    fun makeAdvanced() {
        tipoModelo = "Bikest"
        model = "Mountain Bike"
        numBike = 6
    }
}

fun makeMountainBike(basicBike: Bike): Bike {
    basicBike.makeAdvanced()
    return basicBike
}

fun main(args: Array<String>) {
    val bike = Bike()
    val basicBike = bike.clone()
    val advancedBike = makeMountainBike(basicBike)
    println("Prototype Design Pattern: " + advancedBike.model!!)
}