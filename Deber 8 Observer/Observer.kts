import kotlin.properties.Delegates

interface ValueChangeListener {
    fun onValueChangeListener (newValue:String)
}
class PrintingTextChangedListener: ValueChangeListener {
    override fun onValueChangeListener(newValue: String) {
        println(" Text is changed to:" $newValue)
    }
class ObservableObject (listener: ValueChangeListener)   {
    var text: String by Delegates.observable(
            initialValue = "",
            onChange = {
                prop, old, new ->
                listener.onValueChangeListener(new)
            })
}
    fun main(array: Array<String>) {
        val observableObject = ObservableObject(PrintingTextChangedListener)
        observableObject.text = "Hello"
        observableObject.text = "I'm here"
    }
}